import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    searchText: '',
    modoDark: false,
    peliculas: [],
    pelisFavoritas: []
  },
  mutations: {
    cambiaSearchText(state, texto) {
      state.searchText = texto
    },
    cambiarModo(state) {
      state.modoDark = !state.modoDark;
    },

    llenarPeliculas(state, pelisObtenidas) {
      state.peliculas = pelisObtenidas;
    },
    favoritear(state, index) {
      state.peliculas[index].favorito = !state.peliculas[index].favorito;
      state.pelisFavoritas = state.peliculas.filter(peli => peli.favorito)
    }
  },
  actions: {
    obtenerPeliculas: async function ({ commit }) {
      
      let miVariable = Array(10);
      const nothing = await fetch('https://www.omdbapi.com/?apikey=c74708d&s=*man*')
                  .then(e => e.json())
                  .then(e => miVariable = e.Search)
      // const p = await miVariable.results;
      let contador = -1;
      const pelis = miVariable.map(e => {
        contador++
        return {
          id: contador,
          name: e.Title,
          image: e.Poster,
          favorito: false
        }
      });
      commit('llenarPeliculas', pelis)
    }
  },
  modules: {
  }
})
