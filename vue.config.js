module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: process.env.NODE_ENV === 'production'
      ? '/mis-peliculas/'
      : '/',

  pwa: {
    themeColor: '#003b63',
    manifestOptions: {
      background_color: '#004976'
    }
  }
}